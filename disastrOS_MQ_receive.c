#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"
#include "disastrOS_mqueue.h"
#include "disastrOS_constants.h"

void internal_MQ_receive(){
  char* buffer= (char*)running->syscall_args[0];
  int mq_fd= running->syscall_args[1];

  Descriptor* des=DescriptorList_byFd(&running->descriptors, mq_fd);

  if (! des){
    printf("errore nella ricerca del descrittore nella receive\n");
    running->syscall_retvalue=DSOS_EMQRECEIVE;
    return;
  }
  if(des->mode!=DSOS_READ){
    printf("Il processo %d non può leggere dalla message queue!", des->pcb->pid);
    running->syscall_retvalue=DSOS_EMQRECEIVE;
    return;
  }
  MQ* mq= (MQ*)des->resource;

  if(mq->ml.size == 0){
    printf("coda vuota, il processo con pid=%d deve aspettare!\n", disastrOS_getpid());
    running->status = Waiting;
    List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
    printf("processo %d messo nella waiting list!\n", disastrOS_getpid());
    PCB* next_running= (PCB*) List_detach(&ready_list, ready_list.first);
    running = next_running;
    printf("primo processo nella ready list messo a running!\n");
    return;
  }

  MSG* msg = (MSG*)List_detach(&mq->ml, mq->ml.first);
  if(! msg){
    printf("errore nella rimozione del messaggio dalla message list nella receive\n");
    running->syscall_retvalue=DSOS_EMQRECEIVE;
    return;
  }
  strcpy(buffer, msg->buffer);
  MSG_free(msg);
  running->syscall_retvalue=0;
}