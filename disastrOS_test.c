#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <string.h>

#include "disastrOS.h"
#include "disastrOS_globals.h"

int test;

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}

void childFunction(void* args){
  int pid= disastrOS_getpid();
  int mq_id= *((int*)args);
  int mode;
  if(test==3){
    mode= pid<5? DSOS_READ : DSOS_WRITE ;
  }
  else if(test==4){
    mode= pid<5? DSOS_WRITE : DSOS_READ ;
  }
  else{
    mode= pid%2==0 ? DSOS_READ : DSOS_WRITE ;
  }
  printf("\nHello, I am the child function %d and my args are: mq_id=%d and mode=%d \n", pid, mq_id, mode);
  int type=MQUEUE;
  
  int mq_fd=disastrOS_openResource(mq_id, type, mode);
  char buffer[MAX_MSG_BUFFERSIZE];

  if(mode==DSOS_READ){
    disastrOS_MQ_receive(buffer, mq_fd);
    if(running->syscall_retvalue!=0){
      printf("errore nella receive\n");
      disastrOS_exit(-1);
    }
    printf("Il processo con pid=%d ha ricevuto messaggio:\n'%s'\n", disastrOS_getpid(), buffer);
  }
  else if(mode==DSOS_WRITE){
    int i= snprintf(buffer, MAX_MSG_BUFFERSIZE, "Ciao! Sono il processo con PID=%d, voglio laurearmi :)", disastrOS_getpid());
    printf("Il processo %d vuole inviare alla message queue con id=%d con fd=%d un messaggio di %d bytes...\n", disastrOS_getpid(), mq_id, mq_fd, i);
    disastrOS_MQ_send(buffer, mq_fd);
    if(running->syscall_retvalue!=0){
      printf("errore nella send, retvalue=%d\n", running->syscall_retvalue);
      disastrOS_exit(-1);
    }
    printf("ho scritto il msg correttamente!\n");
  }
  disastrOS_closeResource(mq_fd);
  printf("PID: %d, terminating\n", disastrOS_getpid());
  disastrOS_exit(disastrOS_getpid()+1);
}


void initFunction(void* args) {
  disastrOS_printStatus();
  printf("hello, I am init and I just started\n");

  disastrOS_spawn(sleeperFunction, 0);

  if(test==1){
    printf("Creating/opening a new Message Queue...");
    int mq_id=0;
    int mq_fd= disastrOS_openResource(mq_id, MQUEUE, DSOS_CREATE);

    int n=10;
    printf("I feel like to spawn %d nice threads\n", n);
    int alive_children=0;
    for (int i=0; i<n; i++) {
      disastrOS_spawn(childFunction, (void*)&mq_id);
      alive_children++;
    }

    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
      disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
      pid, retval, alive_children);
      --alive_children;
    }
    disastrOS_closeResource(mq_fd);
    disastrOS_destroyResource(mq_id);
  }
  else if(test==2){
    printf("Creating/opening three new Message Queues...");
    int mq_id_0=0;
    int mq_fd_0= disastrOS_openResource(mq_id_0, MQUEUE, DSOS_CREATE);

    int mq_id_1=1;
    int mq_fd_1= disastrOS_openResource(mq_id_1, MQUEUE, DSOS_CREATE);

    int mq_id_2=2;
    int mq_fd_2= disastrOS_openResource(mq_id_2, MQUEUE, DSOS_CREATE);

    int n=6;
    printf("I feel like to spawn %d nice threads\n", n);
    int alive_children=0;
    for (int i=0; i<n; i++) {
      if(i<2){
        disastrOS_spawn(childFunction, (void*)&mq_id_0);
      }
      else if(i<4){
        disastrOS_spawn(childFunction, (void*)&mq_id_1);
      }
      else{
        disastrOS_spawn(childFunction, (void*)&mq_id_2);
      }
      alive_children++;
    }

    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
      disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
      pid, retval, alive_children);
      --alive_children;
    }
    disastrOS_closeResource(mq_fd_0);
    disastrOS_destroyResource(mq_id_0);
    disastrOS_closeResource(mq_fd_1);
    disastrOS_destroyResource(mq_id_1);
    disastrOS_closeResource(mq_fd_2);
    disastrOS_destroyResource(mq_id_2);
  }
  if(test==3){
    printf("Creating/opening a new Message Queue...");
    int mq_id=0;
    int mq_fd= disastrOS_openResource(mq_id, MQUEUE, DSOS_CREATE);

    int n=10;
    printf("I feel like to spawn %d nice threads\n", n);
    int alive_children=0;
    for (int i=0; i<n; i++) {
      disastrOS_spawn(childFunction, (void*)&mq_id);
      alive_children++;
    }

    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
      disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
      pid, retval, alive_children);
      --alive_children;
    }
    disastrOS_closeResource(mq_fd);
    disastrOS_destroyResource(mq_id);
  }
  if(test==4){
    printf("Creating/opening a new Message Queue...");
    int mq_id=0;
    int mq_fd= disastrOS_openResource(mq_id, MQUEUE, DSOS_CREATE);

    int n=10;
    printf("I feel like to spawn %d nice threads\n", n);
    int alive_children=0;
    for (int i=0; i<n; i++) {
      disastrOS_spawn(childFunction, (void*)&mq_id);
      alive_children++;
    }

    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
      disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
      pid, retval, alive_children);
      --alive_children;
    }
    disastrOS_closeResource(mq_fd);
    disastrOS_destroyResource(mq_id);
  }
  printf("shutdown!\n");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  
  printf("\nScegliere il tipo di test:\n-1=n° di senders uguale al n° di receivers, unica coda\n-2=n° di senders uguale al n° di receivers, code multiple\n-3=n° di senders maggiore del n° di receivers\n-4=n° di senders minore del n° di receivers\n\nMettere il n° associato al test qui: ");
  scanf("%d", &test);
  int correct=0;
  while(!correct){
    if(test==1){
      correct=1;
      printf("\nE' stato scelto il test n° 1!\n\n");
    }
    else if(test==2){
      correct=1;
      printf("\nE' stato scelto il test n° 2!\n\n");
    }
    else if(test==3){
      correct=1;
      printf("\nE' stato scelto il test n° 3!\n\n");
    }
    else if(test==4){
      correct=1;
      printf("\nE' stato scelto il test n° 4!\n\n");
    }
    else{
      printf("Errore! Numero test non corretto, ripetere la scelta: ");
      scanf("%d", &test);
    }
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  printf("the function pointer is: %p", childFunction);
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
